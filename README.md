# Descrição

Este tutorial tem o objetivo de demonstrar como utilizar a pipeline do Gitlab como agente de integração e entrega contpinua dentro do ambiente da Fábrica de Software da Wise.

Neste tutorial, partiremos do projeto criado através dos passos descritos em  [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/).

Além disso, supõe-se que o leitor já tem conhecimento básico sobre o uso do [git](https://git-scm.com/) como controlador de versão de código.


# Objetivo

Ao final deste tutorial, você terá um projeto configurado com integração e entrega contínuos. A versão do sistema disponível na Internet será atualizada automaticamente a cada "push" novo no Gitlab. 

# Criando o projeto

Siga os passos descritos no guia [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/).

Ao final do guia, seu projeto deve rodar sem erros e ser acessível através de http://localhost:8080/greeting com a seguinte resposta:

```json
{"id":1, "content":"Hello, World!"}
```

Na interface web do [Gitlab](https://gitlab.fswise.com.br/), crie um novo projeto pessoal, deixando a opção "Initialize repository with a README" desmarcada. Em seguida, a partir da pasta raíz do projeto na sua máquina, execute os passos:

```bash
git config --global user.name "SEU NOME"
git config --global user.email "SEU EMAIL"
git init
git remote add origin https://gitlab.fswise.com.br/[seu-usuario]/[nome-do-projeto].git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Se tudo correu bem, seu código está salvo e versionado no Gitlab.

# Empacotando como uma imagem Docker

Para que o projeto possa ser empacotado corretamente como uma imagem Docker, é necessária a criação do arquivo `Dockerfile` na pasta raíz do projeto.

`FROM openjdk:8-jdk-alpine` : Uma imagem linux compacta com a JDK 8 já instalada;   
`COPY target/*.jar app.jar`: Copiando o jar compilado para dentro da nossa imagem Docker.   
`ENTRYPOINT ["java","-jar","/app.jar"]`: Qual comando será executado ao rodar a imagem.

Após criar o arquivo com o conteúdo descrito acima, teste a criação da imagem:
```bash
$ docker build -t rest-service .
Sending build context to Docker daemon  220.2kB
Step 1/3 : FROM openjdk:8-jdk-alpine
 ---> a3562aa0b991
Step 2/3 : COPY target/*.jar app.jar
COPY failed: no source files were specified
```

Observe que obtivemos como resultado um erro no passo 2/3. Isso se deve ao fato do jar ainda não ter sido gerado. Para resolver isso, execute:

```bash
❯ mvn clean package
...
[INFO] BUILD SUCCESS
...
```
Verifique se a pastar `target` foi criada e o arquivo .jar está presente dentro dela.

Tente novamente criar a imagem Docker:
```bash
$ docker build -t rest-service .
Sending build context to Docker daemon   16.8MB
Step 1/3 : FROM openjdk:8-jdk-alpine
 ---> a3562aa0b991
Step 2/3 : COPY target/*.jar app.jar
 ---> 041a8c8cd276
Step 3/3 : ENTRYPOINT ["java","-jar","/app.jar"]
 ---> Running in 4e555f4b6e48
Removing intermediate container 4e555f4b6e48
 ---> 71f8932f6ba8
Successfully built 71f8932f6ba8
Successfully tagged rest-service:latest
```

Em seguida verifique sua execução:
```bash
$ docker run -p 8080:8080 rest-service

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.3.4.RELEASE)

2020-01-01 21:45:54.335  INFO 1 --- [           main] br.com.wises.rest.RestApplication        : Starting RestApplication v0.0.1-SNAPSHOT on 278f6a39c858 with PID 1 (/app.jar started by root in /)
...
2020-01-01 21:45:55.824  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
...
```

Verifique se o serviço está disponível em http://localhost:8080/greeting 

Mais informações: 
- [Docker Site](https://www.docker.com/)
- [Spring Boot with Docker](https://spring.io/guides/gs/spring-boot-docker/)


## Opcional
Como medida de segurança, não é recomendado rodar a plaicação como o usuário root. Assim, altere o Dockerfile da seguinte forma:

```yml
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY target/*.jar app.jar`
ENTRYPOINT ["java","-jar","/app.jar"]
```

Recrie sua imagem. Agora sua aplicação vai rodar com o usuário `spring`.

# Configurando a pipeline CI/CD

A configuração da pipeline do Gitlab se faz através do arquivo `.gitlab-ci.yml`.  
Para começar, crie o arquivo com o seguinte conteúdo:

```yml
stages:
  - passo-01

start:
  stage: passo-01
  image: alpine
  script:
    - echo Hello
```

Desta forma, estamos declarando que nossa pipeline terá apenas 1 estágio, chamado `passo-01`. Além disso, declaramos apenas 1 job (`start`), que será executado no `passo-01`, utilizando uma imagem `alpine` (imagem linux compacta) para executar o script definido.

Faça o `commit` e execute o `push` para o Gitlab. Em seguida acesse a interface web do seu projeto no Gitlab, e, na barra lateral esquerda, vá para **CI/CD -> Jobs**. Clique no job listado e observe sua execução.

```
Running with gitlab-runner 13.3.0 (86ad88ea)
  on runner-05 2KGXawxc
Preparing the "docker" executor
Using Docker executor with image alpine ...
Pulling docker image alpine ...
Using docker image sha256:a24bb4013296f61e89ba57005a7b3e52274d8edd3ae2077d04395f806b63d83e for alpine ...
Preparing environment
Running on runner-2kgxawxc-project-330-concurrent-0 via fswrunner10...
Getting source from Git repository
Fetching changes with git depth set to 50...
Initialized empty Git repository in /builds/luiz.rissmann/gitlab-pipeline/.git/
Created fresh repository.
Checking out 630e4cb5 as develop...

Skipping Git submodules setup
Executing "step_script" stage of the job script
$ echo Hello
Hello
Job succeeded
```

Sua pipeline está funcionando. Você pode criar quantos estágios e passos você precisar. 

Mais informações: https://docs.gitlab.com/ee/ci/


# Job de compilação

Vamos alterar a pipeline para que ao invés de `Hello`, ela faça a compilação do projeto. 
Substitua todo o conteúdo do arquivo `.gitlab-ci-yml` por:

```yml
stages:
  - build

compile:
  stage: build
  image: maven:3.6-jdk-8
  script:
    - mvn clean package
```

Agora, sempre que houver um novo `push` para o repositório, a pipeline irá executar o job de compilação automaticamente.

Para otimizar a performance da execução, vamos adicionar um `cache` para as dependências do Maven. Altere o conteúdo do arquivo `.gitlab-ci.yml` para o seguinte:

```yml

variables:
  MAVEN_CLI_OPTS: "-Dmaven.repo.local=repository --batch-mode"

cache:
  key: JAVA_WISE
  paths:
    - repository/

stages:
  - build

compile:
  stage: build
  image: maven:3.6-jdk-8
  script:
    - mvn $MAVEN_CLI_OPTS clean package
```

Em seguida, envie tudo para o `git`:

```bash
$ git add .
$ git commit -m 'Job de compilação'
$ git push
```
Verifique a execução do job na interface web do Gitlab.

# Job de empacotamento

Vamos adicionar um segundo estágio que será responsável por empacotar a aplicação criando uma imagem Docker. Novamente, no arquivo `.gitlab-ci.yml`, adicione o estágio `package` da seguinte forma:

```yml
stages:
  - build
  - package
```

E adicione o job a ser executado ao final do arquivo:
```yml
package-docker:
  stage: package
  image: docker:18-git
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID .
    - docker push $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID
```

Neste job será executado os seguintes comandos:    
- `docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY`: Login no repositório do Gitlab;
- `docker build -t $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID .`: Criação da imagem com a tag referente ao id da pipeline sendo executada;
- `docker push $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID`: Salvamento da imagem criada no repositório do projeto no Gitlab.

Também precisamos fazer uma pequena alteração no job de compilação para que o nosso .jar gerado neste passo esteja disponível no próximo estágio da pipeline. Para isso, adicione as linhas referentes ao artefato a ser mantido:

```yml
compile:
  stage: build
  image: maven:3.6-jdk-8
  script:
    - mvn $MAVEN_CLI_OPTS clean package
  artifacts:
    paths:
      - target/*.jar
    expire_in: 1 week
``` 

Envie tudo para o Gitlab e verifique a execução da pipeline que agora deve conter 2 estágios.    

```bash
$ git add .
$ git commit -m 'Job de empacotamento'
$ git push
```

Ao final da execução com sucesso, acesso o menu `Packages -> Container Registry` para ver sua imagem docker.

## IMPORTANTE

Caso a execução do job termine com o erro:
```
error during connect: Post http://docker:2375/v1.39/auth: dial tcp: lookup docker on 172.30.247.1:53: no such host
ERROR: Job failed: exit code 1
```
Adicione o seguinte trecho ao seu `.gitlab-ci.yml`, logo acima dos `stages`:

```yml
services:
  - name: docker:18-dind
```

Para saber mais sobre Docker-in-Docker: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor


## Variáveis Disponibilizadas pelo Gitlab

O Gitlab nos disponibiliza variáveis pré-definidas que podemos utilizar durante a execução da nossa pipeline. Isso faz com que nosso .gitlab-ci.yml seja facilmente utilizado em outros projetos precisando apenas de ajustes mínimos.
Você pode adicionar a seguinte lista de comandos aos scripts para verificar o conteúdo das variáveis sendo utilizadas:

```yml
  ...
  script:
    - echo $CI_BUILD_TOKEN 
    - echo $CI_REGISTRY
    - echo $CI_REGISTRY_IMAGE
    - echo $CI_PIPELINE_ID
```

Para mais informações sobre as variáveis do Gitlab: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html


# Job de implantação

O Gitlab da Wise já está integrado com um cluster Kubernetes que pode ser utilizado para o deploy das aplicações. Alternativamente, o deploy pode ser feito de outras formas, em outros servidores, alterando o script para que reflita os passos necessários.

Antes de tudo, na interface web do projeto no Gitlab, entre em `Settings -> Repository`, em seguida clique em `Deploy Tokens`. Crie um novo token com o nome `gitlab-deploy-token` e marque o escopo `read_registry`. O Gitlab fará a injeção automática deste token na execução da pipeline para que possamos usá-lo na criação da nossa `secret` do Kubernetes posteriormente.

Para o deploy correto no Kubernetes da Wise, é necessário que algumas configurações iniciais sejam feitas. Para isso, vamos criar um novo estágio na nossa pipeline, chamado `start`:


```yml
stages:
  - build
  - package
  - start
```

E um novo job `start-k8s` responsável pela criação da `secret` contendo as credencias do `Container Registry` para que o Kubernetes possa buscar a imagem diretamente no Gitlab. Neste job também será criado um `deployment`, um `service` e um `ingress` no Kubernetes.


```yml
start-k8s:
  stage: start
  image: roffe/kubectl:v1.13.2
  script:
    - kubectl create secret docker-registry gitlab-image-pull-secret --docker-server=$CI_REGISTRY --docker-email='suporte@wises.com.br' --docker-username=$CI_DEPLOY_USER --docker-password=$CI_DEPLOY_PASSWORD
    - cat k8s/deployment.yml | sed "s|\$CI_PROJECT_NAME|${CI_PROJECT_NAME}|g" | sed "s|\$CI_PIPELINE_ID|${CI_PIPELINE_ID}|g" | sed "s|\$CI_REGISTRY_IMAGE|${CI_REGISTRY_IMAGE}|g" | kubectl apply -f -
    - cat k8s/service.yml | sed "s|\$CI_PROJECT_NAME|${CI_PROJECT_NAME}|g" | sed "s|\$CI_PIPELINE_ID|${CI_PIPELINE_ID}|g" | sed "s|\$CI_REGISTRY_IMAGE|${CI_REGISTRY_IMAGE}|g" | kubectl apply -f -
    - cat k8s/ingress.yml | sed "s|\$CI_PROJECT_NAME|${CI_PROJECT_NAME}|g" | sed "s|\$CI_PIPELINE_ID|${CI_PIPELINE_ID}|g" | sed "s|\$CI_REGISTRY_IMAGE|${CI_REGISTRY_IMAGE}|g" | kubectl apply -f -
    - echo https://$CI_PROJECT_NAME.dev.fswise.com.br
  environment:
    name: dev
    url: https://$CI_PROJECT_NAME.dev.fswise.com.br
  only:
    - start
```

Copie a pasta `k8s` deste tutorial, contendo os 3 arquivos yml de configuração inicial do Kubernetes, e cole na raíz do seu projeto. Não é necessário realizar nenhuma alteração nestes arquivos.

Por ser um assunto muito extenso, não comentaremos a fundo sobre a operação do Kubernetes. Caso tenha interesse, o site oficial disponibiliza uma documentação excelente em: https://kubernetes.io/docs/home/

Envie tudo para o Gitlab e observe a execução da pipeline na interfdace web do seu projeto.

```bash
$ git add .
$ git commit -m 'Job de implantação'
$ git push
```
Note que o job start não foi executado na pipeline. Isso se deve ao fato de termos adicionado uma restrição `only: - start`. Assim, crie uma nova branch com o nome `start` a partir da branch atual e veja que a pipeline completa é disparada automaticamente. 

Esta pipeline deve ser executada apenas uma vez pois inclui a configuração inicial do Kubernetes. Após sua execução, esta branch pode ser excluída do projeto.

Caso a execução termine com o erro abaixo, é provavel que um environment com o mesmo nome já tenha sido criado anteriormente para este projeto.
```
The connection to the server localhost:8080 was refused - did you specify the right host or port?
ERROR: Job failed: exit code 1
```
Para sanar este problema, altere o nome do environment no arquivo `.gitlab-ci.yml` e rode a pipeline novamente.

Ao final da execução da pipeline com sucesso, seu projeto estará implantado e disponível para acesso no endereço:
`https://$CI_PROJECT_NAME.dev.fswise.com.br`


# Entrega Contínua

Agora vamos fazer com que nosso sistema seja atualizado sempre que um novo push for detectado na branch master. Para isso, precisamos apenas atualizar a imagem utilizada no nosso deployment do Kubernetes para a versão mais atual. Vamos implementar isso adicionando um novo estágio na nossa pipeline, que, ao contrário da `start`, será executado sempre.

Adicione ao `.gitlab-ci.yml` o novo estágio:

```yml
stages:
  - build
  - package
  - start
  - deploy
```

E o job que deve ser executado para alterar a imagem sendo executada no Kubernetes:

```yml
deploy:
  stage: deploy
  image: roffe/kubectl:v1.13.2
  script:
    - kubectl set image deployment/$CI_PROJECT_NAME $CI_PROJECT_NAME=$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID
  environment:
    name: dev
```

É importante notar que o deploy depende da definição do environment a ser utilizado. Dessa forma é possível definir deploys diferentes para environments diferentes.

Envie tudo para o Gitlab e observe a execução da pipeline na interfdace web do seu projeto, que deve incluir agora o estágio de deploy.

```bash
$ git add .
$ git commit -m 'Continuous deploy'
$ git push
```

Acesse o sistema através da url `https://$CI_PROJECT_NAMESPACE.dev.fswise.com.br/greeting` e verifique o retorno do nosso GretingController:
```json
{
  "id":1,
  "content":"Hello, World!"
}
```

# Conclusão

A configuração da pipeline de CI/CD do Gitlab pode parecer complicada no início, porém, dificilmente é alterada após sua criação. Além disso, projetos que utilizem a mesma tecnologia podem facilmente compartilhar a mesma configuração, sendo necessário apenas copiar o arquivo `.gitlab-ci.yml`.

Nosso arquivo final, assim como todo código gerado e utilizado neste treinamento, pode ser acessado no repositório deste projeto em: https://gitlab.fswise.com.br/treinamentos/gitlab-ci-example
 